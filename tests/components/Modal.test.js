import Modal from '../../app/components/Modal';

test('Should generate product detail', ()=> {
    const $ = require('jquery');
    $('body').html('').append('<div id="model-container"></div>');

    let modal = new Modal('model-container');
    let dataPromise = {
        _callBack: null,
        then: function (callback) {
            this._callBack = callback;
        },
        makeCall: function (data) {
            this._callBack(data);
        }
    };
    modal.render(dataPromise);
    dataPromise.makeCall({data: ''});
    expect($('#product-modal').length).toBe(1);
    $('#product-modal .product-detail').trigger('click');
});

