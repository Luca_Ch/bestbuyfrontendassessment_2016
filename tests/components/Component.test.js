import Component from '../../app/components/Component';
test('Should create html element', ()=> {
    const $ = require('jquery');
    let MockComponent = Component;
    MockComponent.prototype.init = jest.fn();
    MockComponent.prototype.init.mockReturnValueOnce({
        id: 'test-id',
        tag: 'p',
        className: 'test-class',
        content: 'test-text',
        onClick: () => {
            return true;
        }
    });
    let component = new MockComponent();
    spyOn(component.settings, 'onClick');
    $('body').append('<div id="container-id"></div>');
    let element = component.render({containerId: 'container-id'});
    expect(element.className).toBe('test-class');
    expect(element.tagName).toBe('P');
    expect(element.innerHTML).toBe('test-text');
    // Use jquery to emulate a click
    $('#test-id').click();
    expect(component.settings.onClick).toHaveBeenCalled();

});


