/**
 * Mock jsonp module
 */
jest.mock('../../app/services/jsonp', () => {
    return jest.fn((url) => {
            if (url.indexOf('/category/Departments') != -1) {
                return new Promise((resolve, reject) => {
                    resolve({subCategories: {data: 'test'}});
                });
            } else if (url.indexOf('/search?categoryId=cat001') != -1) {
                return new Promise((resolve, reject) => {
                    resolve({products: {data: 'test'}});
                });
            } else if (url.indexOf('/product/product001') != -1) {
                return new Promise((resolve, reject) => {
                    resolve({data: 'test'});
                });
            }
        }
    );
});

import dataService from '../../app/services/DataService';

test('Should return categories data', ()=> {

    return dataService.getCategories()
        .then(data => {
                expect(data).toEqual({data: 'test'});
            }
        );
});

test('Should return products for category cat001', ()=> {

    return dataService.getCategoryProducts('cat001')
        .then(data => {
                expect(data).toEqual({data: 'test'});
            }
        );
});

test('Should return product detail for product001', ()=> {

    return dataService.getProductInformation('product001')
        .then(data => {
                expect(data).toEqual({data: 'test'});
            }
        );
});
