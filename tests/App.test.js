import App from '../app/App';
import DataService from '../app/services/DataService';
/**
 * Mock DataService
 */
jest.mock('../app/services/DataService', () => {
    let allCategoriesPromise = {
        _callBack: null,
        then: function (callback) {
            this._callBack = callback;
        },
        makeCall: function (data) {
            this._callBack(data);
        }
    };
    let CategoryProductsPromise = {
        _callBack: null,
        then: function (callback) {
            this._callBack = callback;
        },
        makeCall: function (data) {
            this._callBack(data);
        }
    };
    let CategoryAllProductsPromise = {
        _callBack: null,
        then: function (callback) {
            this._callBack = callback;
        },
        makeCall: function (data) {
            this._callBack(data);
        }
    };
    let ProductDetailPromise = {
        _callBack: null,
        then: function (callback) {
            this._callBack = callback;
        },
        makeCall: function (data) {
            this._callBack(data);
        }
    };
    return {
        getCategories: function () {

            return allCategoriesPromise;
        },
        getCategoryProducts: function (category) {
            if (category === 'departments') {
                return CategoryAllProductsPromise;
            } else {
                return CategoryProductsPromise;
            }
        },
        getProductInformation: function () {
            return ProductDetailPromise;
        },
        callBackGetCategories: function (data) {
            allCategoriesPromise.makeCall(data);
        },
        callBackGetAllCategoryProducts: function (data) {
            CategoryAllProductsPromise.makeCall(data);
        },
        callBackGetCategoryProducts: function (data) {
            CategoryProductsPromise.makeCall(data);
        },
        callBackGetProductInformation: function (data) {
            ProductDetailPromise.makeCall(data);
        }
    };

});


const allProducts = [
    {
        sku: 'prod001',
        name: 'Product1',
        thumbnailImage: 'htp:/test/Product1.jpg',
        shortDescription: ' Product 1 shortDescription',
        regularPrice: 100
    },
    {
        sku: 'prod002',
        name: 'Product2',
        thumbnailImage: 'htp:/test/Product2.jpg',
        shortDescription: ' Product 2 shortDescription',
        regularPrice: 130
    },
    {
        sku: 'prod003',
        name: 'Product3',
        thumbnailImage: 'htp:/test/Product3.jpg',
        shortDescription: ' Product 3 shortDescription',
        regularPrice: 150
    }
];
const allCategories = [
    {id: 'cat01', name: 'Category 1'},
    {id: 'cat02', name: 'Category 2'},
    {id: 'cat03', name: 'Category 3'}

];
const $ = require('jquery');

test('Should load three categories and three products', ()=> {

    $('body').html('').append('<div id="app-container"></div>');

    App.render('app-container');
    expect($('#categories').length).toBe(1);
    expect($('#products').length).toBe(1);
    expect($('#modal').length).toBe(1);
    DataService.callBackGetCategories(allCategories);
    expect($('#categories .category-item').length).toBe(3);
    let cat2 = $($('#categories .category-item')[1]);
    expect(cat2.html()).toBe(allCategories[1].name);

    DataService.callBackGetAllCategoryProducts(allProducts);
    expect($('#products .product-item').length).toBe(3);
    let product2 = $($('#products .product-item')[1]);
    expect(product2.find('.product-image img')[0].src).toBe(allProducts[1].thumbnailImage);
    expect(product2.find('.product-image img')[0].alt).toBe(allProducts[1].name);
    expect(product2.find('.product-name').html()).toBe(allProducts[1].name);
    expect(product2.find('.product-price strong').html()).toBe('$' + allProducts[1].regularPrice);


    cat2.trigger('click');

});

test('Should display ony product003 for category cat02 ', ()=> {

    $('body').html('').append('<div id="app-container"></div>');
    App.render('app-container');
    DataService.callBackGetCategories(allCategories);
    DataService.callBackGetAllCategoryProducts(allProducts);
    let cat2 = $($('#categories .category-item')[1]);
    cat2.trigger('click');
    DataService.callBackGetCategoryProducts([allProducts[2]]);
    expect($('#products .product-item').length).toBe(1);
    expect($('#products .product-item .product-image img')[0].src).toBe(allProducts[2].thumbnailImage);
    expect($('#products .product-item .product-image img')[0].alt).toBe(allProducts[2].name);
    expect($('#products .product-item .product-name').html()).toBe(allProducts[2].name);
    expect($('#products .product-item .product-price strong').html()).toBe('$' + allProducts[2].regularPrice);

});

test('Should display product detail for product002 ', ()=> {
    $('body').html('').append('<div id="app-container"></div>');
    App.render('app-container');
    DataService.callBackGetCategories(allCategories);
    DataService.callBackGetAllCategoryProducts(allProducts);
    let product2 = $($('#products .product-item')[1]);
    product2.trigger('click');
    DataService.callBackGetProductInformation(allProducts[2]);
    expect($('#product-modal').length).toBe(1);
    expect($('#product-modal .product-name h1').html()).toBe(allProducts[2].name);
    expect($('#product-modal .product-image img')[0].alt).toBe(allProducts[2].name);
    expect($('#product-modal .product-description').html()).toBe(allProducts[2].shortDescription);
    expect($('#product-modal .product-price h2').html()).toBe('$' + allProducts[2].regularPrice);
});