# BBYC - MVC ASSESMENT
  
Pure Javascript Object Oriented Application with ES2015 features 

## Getting started

To run this application, you'll need:

1. [Node.js](http://nodejs.org) installed

## Running BBYC - MVC

### 1.Install project dependency library

*To transpile ES2015 modules into regular es5 Javascript and bundle them into single file, I used :*  
* babelify
* browserify
* watchify 

*To run this application local I used:*
     
* express 

*And for unit tests:*

* jest
* jquery

```sh

cd <project folder>

npm install

```
  
 ### 2.Compile javaScript code form es6 code into es5
```sh

cd <project folder>

npm start

```

### 3. Run Application on local server

```sh

cd <project folder>

node server/server.js

```


### 3. Run JavaScript unit test

  
```sh

cd <project folder>

jest --coverage

or 

npm jest --coverage

```

## Design Patterns

1. I used **modules** form ES2015 which allow use to isolate code into separate files.
2. **Classes** as well allow isolate and have ability easy extend the code.
3. **Service** allow having access into API in any place of this application.
4. For Application  and  Service,  I used **singleton** pattern.
5. I used  **Promise** for async work with API and HTML generation.
 
 
## Additional information
I am personally to prefer to use latest JavaScript framework **AngularJS 2.x** or **ReactJS + Redux** because a lot features such as routing, template generation, HTTP utility, and in React Virtual DOM, and many others which allow making you development much easy and faster.
       
 