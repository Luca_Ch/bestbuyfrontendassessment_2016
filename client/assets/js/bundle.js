(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _DataService = require('./services/DataService');

var _DataService2 = _interopRequireDefault(_DataService);

var _Component2 = require('./components/Component');

var _Component3 = _interopRequireDefault(_Component2);

var _Category = require('./components/Category');

var _Category2 = _interopRequireDefault(_Category);

var _Product = require('./components/Product');

var _Product2 = _interopRequireDefault(_Product);

var _ListView = require('./components/ListView');

var _ListView2 = _interopRequireDefault(_ListView);

var _Modal = require('./components/Modal');

var _Modal2 = _interopRequireDefault(_Modal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Application class
 *
 */
var App = function (_Component) {
    _inherits(App, _Component);

    function App() {
        _classCallCheck(this, App);

        return _possibleConstructorReturn(this, (App.__proto__ || Object.getPrototypeOf(App)).apply(this, arguments));
    }

    _createClass(App, [{
        key: 'init',


        /**
         * Component configuration.
         *
         * @returns {{content: string}}
         */
        value: function init() {
            return {
                content: '\n        <div id="categories"></div>\n\t\t<div id="products"></div>\n\t\t<div id="modal"></div>'
            };
        }

        /**
         * Generate HTML element.
         *
         * @param {string} containerId - Container Id for application.
         */

    }, {
        key: 'render',
        value: function render(containerId) {
            _get(App.prototype.__proto__ || Object.getPrototypeOf(App.prototype), 'render', this).call(this, { containerId: containerId });
            var modal = new _Modal2.default('modal');
            var productLists = new _ListView2.default('products', _Product2.default, function (product) {
                modal.render(_DataService2.default.getProductInformation(product.getValue('sku')));
            });
            var sideBarList = new _ListView2.default('categories', _Category2.default, function (category) {
                productLists.render(_DataService2.default.getCategoryProducts(category.getValue('id')));
            });

            var allCategories = _DataService2.default.getCategories();
            var allProducts = _DataService2.default.getCategoryProducts('departments');

            sideBarList.render(allCategories);
            productLists.render(allProducts);
        }
    }]);

    return App;
}(_Component3.default);

var app = new App();
exports.default = app;

},{"./components/Category":2,"./components/Component":3,"./components/ListView":4,"./components/Modal":5,"./components/Product":6,"./services/DataService":9}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Component2 = require('./Component');

var _Component3 = _interopRequireDefault(_Component2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Category component class
 * will create category list item
 *
 */
var Category = function (_Component) {
    _inherits(Category, _Component);

    function Category() {
        _classCallCheck(this, Category);

        return _possibleConstructorReturn(this, (Category.__proto__ || Object.getPrototypeOf(Category)).apply(this, arguments));
    }

    _createClass(Category, [{
        key: 'init',
        value: function init() {
            return {
                content: this.getValue('name'),
                className: 'category-item'
            };
        }
    }]);

    return Category;
}(_Component3.default);

exports.default = Category;

},{"./Component":3}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Main class component class
 */
var Component = function () {

    /**
     * Component constructor
     *
     * @param {{}}       data     - Component data.
     * @param {function} callback - Click callback function
     */
    function Component(data) {
        var callback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

        _classCallCheck(this, Component);

        var defaults = {
            tag: 'div',
            className: '',
            content: '',
            onClick: callback
        };
        this.data = data;
        this.settings = Object.assign({}, defaults, this.init());
    }

    /**
     * Extra component configuration.
     *
     * @returns {{}}
     */


    _createClass(Component, [{
        key: 'init',
        value: function init() {
            return {};
        }

        /**
         * Get data value
         *
         * @param {string} key - The key name.
         *
         * @returns {*}
         */

    }, {
        key: 'getValue',
        value: function getValue(key) {
            return this.data[key];
        }

        /**
         * Generate HTML element.
         *
         * @param {string}  containerId - Container Id for this component.
         * @param {boolean} append      - Append flag allow add to exist content.
         *
         * @returns {Element}
         */

    }, {
        key: 'render',
        value: function render() {
            var _this = this;

            var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

            var containerId = _ref.containerId;
            var _ref$append = _ref.append;
            var append = _ref$append === undefined ? false : _ref$append;

            var element = document.createElement(this.settings.tag);
            element.innerHTML = this.settings.content;
            element.className = this.settings.className;
            if (typeof this.settings.id !== 'undefined') {
                element.id = this.settings.id;
            }
            if (this.settings.onClick != null) {
                element[window.addEventListener ? 'addEventListener' : 'attachEvent'](window.addEventListener ? 'click' : 'onclick', function (event) {
                    var _settings;

                    (_settings = _this.settings).onClick.apply(_settings, [_this, event]);
                }, false);
            }
            if (typeof containerId !== 'undefined') {
                var container = document.getElementById(containerId);
                if (!append) {
                    container.innerHTML = '';
                }
                container.appendChild(element);
            }

            return element;
        }

        /**
         * Stop event propagation.
         *
         * @param event
         */

    }, {
        key: 'stopPropagation',
        value: function stopPropagation(event) {
            if (event.stopPropagation) event.stopPropagation();else event.cancelBubble = true;
        }
    }]);

    return Component;
}();

exports.default = Component;

},{}],4:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * List View class
 */
var ListView = function () {
    /**
     * List view constructor.
     *
     * @param {string}   containerId - Container Id for this list view.
     * @param {string}   itemClass   - Item's class for this list view.
     * @param {function} callBack    - callback event function.
     */
    function ListView(containerId, itemClass, callBack) {
        _classCallCheck(this, ListView);

        this.itemClass = itemClass;
        this.containerId = containerId;
        this.callBack = callBack;
    }

    /**
     * Generate HTML element with nodes of components base on itemClass
     *
     * @param {Promise} dataPromise
     */


    _createClass(ListView, [{
        key: 'render',
        value: function render(dataPromise) {
            var _this = this;

            var container = document.getElementById(this.containerId);
            container.innerHTML = '<div class="loader"></div>';
            dataPromise.then(function (data) {
                container.innerHTML = '';
                data.map(function (item) {
                    var _item = new _this.itemClass(item, _this.callBack);
                    container.appendChild(_item.render());
                });
            });
        }
    }]);

    return ListView;
}();

exports.default = ListView;

},{}],5:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _Component2 = require('./Component');

var _Component3 = _interopRequireDefault(_Component2);

var _ProductDetail = require('./ProductDetail');

var _ProductDetail2 = _interopRequireDefault(_ProductDetail);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Product Modal View.
 */
var Modal = function (_Component) {
    _inherits(Modal, _Component);

    /**
     * Product Modal constructor.
     *
     * @param {string} containerId - Container Id for this Modal.
     */
    function Modal(containerId) {
        _classCallCheck(this, Modal);

        var _this = _possibleConstructorReturn(this, (Modal.__proto__ || Object.getPrototypeOf(Modal)).call(this));

        _this.containerId = containerId;
        return _this;
    }

    /**
     * Component configuration
     *
     * @returns {{id: string, content: string, onClick: (function())}}
     */


    _createClass(Modal, [{
        key: 'init',
        value: function init() {
            var _this2 = this;

            return {
                id: 'overlay',
                content: '<div id="product-modal"><div class="loader"></div></div>',
                onClick: function onClick() {
                    document.getElementById(_this2.containerId).innerHTML = '';
                }
            };
        }

        /**
         * Generate HTML element.
         *
         * @param {Promise} dataPromise
         */

    }, {
        key: 'render',
        value: function render(dataPromise) {
            var _this3 = this;

            _get(Modal.prototype.__proto__ || Object.getPrototypeOf(Modal.prototype), 'render', this).call(this, { containerId: this.containerId });
            dataPromise.then(function (data) {
                var productDetail = new _ProductDetail2.default(data, function (a, e) {
                    _this3.stopPropagation(e);
                });
                productDetail.render({ containerId: 'product-modal' });
            });
        }
    }]);

    return Modal;
}(_Component3.default);

exports.default = Modal;

},{"./Component":3,"./ProductDetail":7}],6:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Component2 = require('./Component');

var _Component3 = _interopRequireDefault(_Component2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Product class
 */
var Product = function (_Component) {
    _inherits(Product, _Component);

    function Product() {
        _classCallCheck(this, Product);

        return _possibleConstructorReturn(this, (Product.__proto__ || Object.getPrototypeOf(Product)).apply(this, arguments));
    }

    _createClass(Product, [{
        key: 'init',

        /**
         * Component configuration.
         *
         * @returns {{content: string, className: string}}
         */
        value: function init() {
            return {
                content: '<div class="product-image">\n                            <img src="' + this.getValue('thumbnailImage') + '" alt="' + this.getValue('name') + '">\n                            </div>\n                            <div class="product-name">' + this.getValue('name') + '</div>\n                            <div class="product-price"><strong>$' + this.getValue('regularPrice') + '</strong></div>\n                            ',
                className: 'product-item'
            };
        }
    }]);

    return Product;
}(_Component3.default);

exports.default = Product;

},{"./Component":3}],7:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Component2 = require('./Component');

var _Component3 = _interopRequireDefault(_Component2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * ProductDetail class
 */
var ProductDetail = function (_Component) {
    _inherits(ProductDetail, _Component);

    function ProductDetail() {
        _classCallCheck(this, ProductDetail);

        return _possibleConstructorReturn(this, (ProductDetail.__proto__ || Object.getPrototypeOf(ProductDetail)).apply(this, arguments));
    }

    _createClass(ProductDetail, [{
        key: 'init',

        /**
         * Component configuration.
         *
         * @returns {{content: string, className: string}}
         */
        value: function init() {
            return {
                content: '<div class="product-name"><h1>' + this.getValue('name') + '</h1></div>\n                      <div class="product-image">\n                        <img src="' + this.getValue('thumbnailImage') + '" alt="' + this.getValue('name') + '">\n                      </div>\n                      <div class="product-description">' + this.getValue('shortDescription') + '</div>\n                      <div class="product-price"><h2>$' + this.getValue('regularPrice') + '</h2></div>\n                       ',
                className: 'product-detail'
            };
        }
    }]);

    return ProductDetail;
}(_Component3.default);

exports.default = ProductDetail;

},{"./Component":3}],8:[function(require,module,exports){
'use strict';

var _App = require('./App');

var _App2 = _interopRequireDefault(_App);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_App2.default.render('app-container');

},{"./App":1}],9:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jsonp = require('./jsonp');

var _jsonp2 = _interopRequireDefault(_jsonp);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Base API Url
 * @type {string}
 */
var BASE_URL = 'http://www.bestbuy.ca/api/v2/json';

/**
 * DataService class.
 */

var DataService = function () {
    function DataService() {
        _classCallCheck(this, DataService);
    }

    _createClass(DataService, [{
        key: 'getCategories',


        /**
         * Get All categories.
         *
         * @returns {Promise}
         */
        value: function getCategories() {

            return new Promise(function (resolve, reject) {
                (0, _jsonp2.default)(BASE_URL + '/category/Departments').then(function (response) {
                    return resolve(response.subCategories);
                });
            });
        }

        /**
         * Get category products.
         *
         * @param {string} categoryId - The category id
         *
         * @returns {Promise}
         */

    }, {
        key: 'getCategoryProducts',
        value: function getCategoryProducts(categoryId) {

            return new Promise(function (resolve, reject) {
                (0, _jsonp2.default)(BASE_URL + '/search?categoryId=' + categoryId).then(function (response) {
                    return resolve(response.products);
                });
            });
        }

        /**
         * Get product Information.
         *
         * @param {string} productId = The product Id.
         *
         * @returns {Promise}
         */

    }, {
        key: 'getProductInformation',
        value: function getProductInformation(productId) {

            return new Promise(function (resolve, reject) {
                (0, _jsonp2.default)(BASE_URL + '/product/' + productId).then(function (response) {
                    return resolve(response);
                });
            });
        }
    }]);

    return DataService;
}();

var SingletonDataService = new DataService();
exports.default = SingletonDataService;

},{"./jsonp":10}],10:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = jsonp;
/**
 * Ajax request utility
 *
 * @param {string} uri - The resource uri
 *
 * @returns {Promise}
 */
function jsonp(uri) {
    return new Promise(function (resolve, reject) {

        var id = '_' + Math.round(10000 * Math.random());
        var callbackName = 'jsonp_callback_' + id;
        window[callbackName] = function (data) {
            resolve(data);
            window[callbackName] = undefined;
        };

        var ref = window.document.getElementsByTagName('script')[0];
        var script = window.document.createElement('script');
        script.src = uri + (uri.indexOf('?') + 1 ? '&' : '?') + 'callback=' + callbackName;

        ref.parentNode.insertBefore(script, ref);

        script.onload = function () {
            this.remove();
        };
    });
}

},{}]},{},[8])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJhcHAvQXBwLmpzIiwiYXBwL2NvbXBvbmVudHMvQ2F0ZWdvcnkuanMiLCJhcHAvY29tcG9uZW50cy9Db21wb25lbnQuanMiLCJhcHAvY29tcG9uZW50cy9MaXN0Vmlldy5qcyIsImFwcC9jb21wb25lbnRzL01vZGFsLmpzIiwiYXBwL2NvbXBvbmVudHMvUHJvZHVjdC5qcyIsImFwcC9jb21wb25lbnRzL1Byb2R1Y3REZXRhaWwuanMiLCJhcHAvbWFpbi5qcyIsImFwcC9zZXJ2aWNlcy9EYXRhU2VydmljZS5qcyIsImFwcC9zZXJ2aWNlcy9qc29ucC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7QUNBQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7Ozs7Ozs7OztBQUVBOzs7O0lBSU0sRzs7Ozs7Ozs7Ozs7OztBQUVGOzs7OzsrQkFLTTtBQUNGLG1CQUFPO0FBQ0g7QUFERyxhQUFQO0FBTUg7O0FBRUQ7Ozs7Ozs7OytCQUtPLFcsRUFBWTtBQUNmLDZHQUFhLEVBQUMsd0JBQUQsRUFBYjtBQUNBLGdCQUFJLFFBQVEsb0JBQVUsT0FBVixDQUFaO0FBQ0EsZ0JBQUksZUFBYyx1QkFBYSxVQUFiLHFCQUFnQyxVQUFDLE9BQUQsRUFBVztBQUN6RCxzQkFBTSxNQUFOLENBQWEsc0JBQVkscUJBQVosQ0FBa0MsUUFBUSxRQUFSLENBQWlCLEtBQWpCLENBQWxDLENBQWI7QUFDSCxhQUZpQixDQUFsQjtBQUdBLGdCQUFJLGNBQWEsdUJBQWEsWUFBYixzQkFBbUMsVUFBQyxRQUFELEVBQVk7QUFDNUQsNkJBQWEsTUFBYixDQUFvQixzQkFBWSxtQkFBWixDQUFnQyxTQUFTLFFBQVQsQ0FBa0IsSUFBbEIsQ0FBaEMsQ0FBcEI7QUFDSCxhQUZnQixDQUFqQjs7QUFJQSxnQkFBSSxnQkFBZSxzQkFBWSxhQUFaLEVBQW5CO0FBQ0EsZ0JBQUksY0FBYSxzQkFBWSxtQkFBWixDQUFnQyxhQUFoQyxDQUFqQjs7QUFFQSx3QkFBWSxNQUFaLENBQW1CLGFBQW5CO0FBQ0EseUJBQWEsTUFBYixDQUFvQixXQUFwQjtBQUNIOzs7Ozs7QUFFTCxJQUFJLE1BQU0sSUFBSSxHQUFKLEVBQVY7a0JBQ2UsRzs7Ozs7Ozs7Ozs7QUNsRGY7Ozs7Ozs7Ozs7OztBQUNBOzs7OztJQUtxQixROzs7Ozs7Ozs7OzsrQkFDVjtBQUNKLG1CQUFNO0FBQ0YseUJBQVMsS0FBSyxRQUFMLENBQWMsTUFBZCxDQURQO0FBRUYsMkJBQVU7QUFGUixhQUFOO0FBSUg7Ozs7OztrQkFOaUIsUTs7Ozs7Ozs7Ozs7OztBQ05yQjs7O0lBR3FCLFM7O0FBRWpCOzs7Ozs7QUFNQSx1QkFBWSxJQUFaLEVBQW1DO0FBQUEsWUFBakIsUUFBaUIsdUVBQU4sSUFBTTs7QUFBQTs7QUFDL0IsWUFBSSxXQUFXO0FBQ1gsaUJBQUssS0FETTtBQUVYLHVCQUFXLEVBRkE7QUFHWCxxQkFBUyxFQUhFO0FBSVgscUJBQVM7QUFKRSxTQUFmO0FBTUEsYUFBSyxJQUFMLEdBQVksSUFBWjtBQUNBLGFBQUssUUFBTCxHQUFnQixPQUFPLE1BQVAsQ0FBYyxFQUFkLEVBQWtCLFFBQWxCLEVBQTRCLEtBQUssSUFBTCxFQUE1QixDQUFoQjtBQUNIOztBQUVEOzs7Ozs7Ozs7K0JBS087QUFDSCxtQkFBTyxFQUFQO0FBQ0g7O0FBRUQ7Ozs7Ozs7Ozs7aUNBT1MsRyxFQUFLO0FBQ1YsbUJBQU8sS0FBSyxJQUFMLENBQVUsR0FBVixDQUFQO0FBQ0g7O0FBRUQ7Ozs7Ozs7Ozs7O2lDQVF5QztBQUFBOztBQUFBLDJGQUFKLEVBQUk7O0FBQUEsZ0JBQWpDLFdBQWlDLFFBQWpDLFdBQWlDO0FBQUEsbUNBQXBCLE1BQW9CO0FBQUEsZ0JBQXBCLE1BQW9CLCtCQUFYLEtBQVc7O0FBQ3JDLGdCQUFJLFVBQVUsU0FBUyxhQUFULENBQXVCLEtBQUssUUFBTCxDQUFjLEdBQXJDLENBQWQ7QUFDQSxvQkFBUSxTQUFSLEdBQW9CLEtBQUssUUFBTCxDQUFjLE9BQWxDO0FBQ0Esb0JBQVEsU0FBUixHQUFvQixLQUFLLFFBQUwsQ0FBYyxTQUFsQztBQUNBLGdCQUFJLE9BQU8sS0FBSyxRQUFMLENBQWMsRUFBckIsS0FBNEIsV0FBaEMsRUFBNkM7QUFDekMsd0JBQVEsRUFBUixHQUFhLEtBQUssUUFBTCxDQUFjLEVBQTNCO0FBQ0g7QUFDRCxnQkFBSSxLQUFLLFFBQUwsQ0FBYyxPQUFkLElBQXlCLElBQTdCLEVBQW1DO0FBQy9CLHdCQUFRLE9BQU8sZ0JBQVAsR0FBMEIsa0JBQTFCLEdBQStDLGFBQXZELEVBQXNFLE9BQU8sZ0JBQVAsR0FBMEIsT0FBMUIsR0FBb0MsU0FBMUcsRUFBcUgsVUFBQyxLQUFELEVBQVc7QUFBQTs7QUFDNUgsdUNBQUssUUFBTCxFQUFjLE9BQWQsa0JBQXlCLFFBQU8sS0FBUCxDQUF6QjtBQUNILGlCQUZELEVBRUcsS0FGSDtBQUdIO0FBQ0QsZ0JBQUksT0FBTyxXQUFQLEtBQXVCLFdBQTNCLEVBQXdDO0FBQ3BDLG9CQUFJLFlBQVksU0FBUyxjQUFULENBQXdCLFdBQXhCLENBQWhCO0FBQ0Esb0JBQUksQ0FBQyxNQUFMLEVBQWE7QUFDVCw4QkFBVSxTQUFWLEdBQXNCLEVBQXRCO0FBQ0g7QUFDRCwwQkFBVSxXQUFWLENBQXNCLE9BQXRCO0FBQ0g7O0FBRUQsbUJBQU8sT0FBUDtBQUNIOztBQUVEOzs7Ozs7Ozt3Q0FLZ0IsSyxFQUFPO0FBQ25CLGdCQUFJLE1BQU0sZUFBVixFQUEwQixNQUFNLGVBQU4sR0FBMUIsS0FDSyxNQUFNLFlBQU4sR0FBcUIsSUFBckI7QUFDUjs7Ozs7O2tCQTlFZ0IsUzs7Ozs7Ozs7Ozs7OztBQ0hyQjs7O0lBR3FCLFE7QUFDakI7Ozs7Ozs7QUFPQSxzQkFBWSxXQUFaLEVBQXlCLFNBQXpCLEVBQW9DLFFBQXBDLEVBQThDO0FBQUE7O0FBQzFDLGFBQUssU0FBTCxHQUFpQixTQUFqQjtBQUNBLGFBQUssV0FBTCxHQUFtQixXQUFuQjtBQUNBLGFBQUssUUFBTCxHQUFnQixRQUFoQjtBQUNIOztBQUVEOzs7Ozs7Ozs7K0JBS08sVyxFQUFhO0FBQUE7O0FBQ2hCLGdCQUFJLFlBQVksU0FBUyxjQUFULENBQXdCLEtBQUssV0FBN0IsQ0FBaEI7QUFDQSxzQkFBVSxTQUFWLEdBQXNCLDRCQUF0QjtBQUNBLHdCQUFZLElBQVosQ0FBaUIsVUFBQyxJQUFELEVBQVM7QUFDbEIsMEJBQVUsU0FBVixHQUFzQixFQUF0QjtBQUNBLHFCQUFLLEdBQUwsQ0FBUyxnQkFBUTtBQUNiLHdCQUFJLFFBQVEsSUFBSSxNQUFLLFNBQVQsQ0FBbUIsSUFBbkIsRUFBeUIsTUFBSyxRQUE5QixDQUFaO0FBQ0EsOEJBQVUsV0FBVixDQUFzQixNQUFNLE1BQU4sRUFBdEI7QUFDSCxpQkFIRDtBQUlILGFBTkw7QUFRSDs7Ozs7O2tCQTlCZ0IsUTs7Ozs7Ozs7Ozs7OztBQ0hyQjs7OztBQUNBOzs7Ozs7Ozs7Ozs7QUFFQTs7O0lBR3FCLEs7OztBQUVqQjs7Ozs7QUFLQSxtQkFBWSxXQUFaLEVBQXlCO0FBQUE7O0FBQUE7O0FBRXJCLGNBQUssV0FBTCxHQUFtQixXQUFuQjtBQUZxQjtBQUd4Qjs7QUFFRDs7Ozs7Ozs7OytCQUtPO0FBQUE7O0FBQ0gsbUJBQU87QUFDSCxvQkFBSSxTQUREO0FBRUgsbUZBRkc7QUFHSCx5QkFBUyxtQkFBSztBQUNWLDZCQUFTLGNBQVQsQ0FBd0IsT0FBSyxXQUE3QixFQUEwQyxTQUExQyxHQUFzRCxFQUF0RDtBQUNIO0FBTEUsYUFBUDtBQU9IOztBQUVEOzs7Ozs7OzsrQkFLTyxXLEVBQWE7QUFBQTs7QUFDaEIsaUhBQWEsRUFBQyxhQUFhLEtBQUssV0FBbkIsRUFBYjtBQUNBLHdCQUFZLElBQVosQ0FBaUIsVUFBQyxJQUFELEVBQVM7QUFDbEIsb0JBQUksZ0JBQWdCLDRCQUFrQixJQUFsQixFQUF3QixVQUFDLENBQUQsRUFBSSxDQUFKLEVBQVM7QUFDakQsMkJBQUssZUFBTCxDQUFxQixDQUFyQjtBQUNILGlCQUZtQixDQUFwQjtBQUdBLDhCQUFjLE1BQWQsQ0FBcUIsRUFBQyxhQUFhLGVBQWQsRUFBckI7QUFDSCxhQUxMO0FBT0g7Ozs7OztrQkF6Q2dCLEs7Ozs7Ozs7Ozs7O0FDTnJCOzs7Ozs7Ozs7Ozs7QUFDQTs7O0lBR3FCLE87Ozs7Ozs7Ozs7OztBQUNqQjs7Ozs7K0JBS087QUFDSCxtQkFBTztBQUNILGlHQUM0QixLQUFLLFFBQUwsQ0FBYyxnQkFBZCxDQUQ1QixlQUNxRSxLQUFLLFFBQUwsQ0FBYyxNQUFkLENBRHJFLHNHQUc0QyxLQUFLLFFBQUwsQ0FBYyxNQUFkLENBSDVDLGdGQUlzRCxLQUFLLFFBQUwsQ0FBYyxjQUFkLENBSnRELGtEQURHO0FBT0gsMkJBQVc7QUFQUixhQUFQO0FBU0g7Ozs7OztrQkFoQmdCLE87Ozs7Ozs7Ozs7O0FDSnJCOzs7Ozs7Ozs7Ozs7QUFDQTs7O0lBR3FCLGE7Ozs7Ozs7Ozs7OztBQUNqQjs7Ozs7K0JBS087QUFDSCxtQkFBTztBQUNILDREQUEwQyxLQUFLLFFBQUwsQ0FBYyxNQUFkLENBQTFDLDBHQUV3QixLQUFLLFFBQUwsQ0FBYyxnQkFBZCxDQUZ4QixlQUVpRSxLQUFLLFFBQUwsQ0FBYyxNQUFkLENBRmpFLGlHQUk2QyxLQUFLLFFBQUwsQ0FBYyxrQkFBZCxDQUo3QyxzRUFLNEMsS0FBSyxRQUFMLENBQWMsY0FBZCxDQUw1Qyx5Q0FERztBQVFILDJCQUFXO0FBUlIsYUFBUDtBQVVIOzs7Ozs7a0JBakJnQixhOzs7OztBQ0pyQjs7Ozs7O0FBQ0EsY0FBSSxNQUFKLENBQVcsZUFBWDs7Ozs7Ozs7Ozs7QUNEQTs7Ozs7Ozs7QUFDQTs7OztBQUlBLElBQU0sV0FBUyxtQ0FBZjs7QUFFQTs7OztJQUdNLFc7Ozs7Ozs7OztBQUVGOzs7Ozt3Q0FLZTs7QUFFWCxtQkFBTyxJQUFJLE9BQUosQ0FBWSxVQUFDLE9BQUQsRUFBVSxNQUFWLEVBQXFCO0FBQ3BDLHFDQUFTLFFBQVQsNEJBQTBDLElBQTFDLENBQStDLG9CQUFZO0FBQ25ELDJCQUFPLFFBQVEsU0FBUyxhQUFqQixDQUFQO0FBQ0gsaUJBRkw7QUFHSCxhQUpNLENBQVA7QUFLSDs7QUFFRDs7Ozs7Ozs7Ozs0Q0FPb0IsVSxFQUFXOztBQUUzQixtQkFBTyxJQUFJLE9BQUosQ0FBWSxVQUFDLE9BQUQsRUFBVSxNQUFWLEVBQXFCO0FBQ3BDLHFDQUFTLFFBQVQsMkJBQXVDLFVBQXZDLEVBQXFELElBQXJELENBQTBELG9CQUFZO0FBQ2xFLDJCQUFPLFFBQVEsU0FBUyxRQUFqQixDQUFQO0FBQ0gsaUJBRkQ7QUFHSCxhQUpNLENBQVA7QUFLSDs7QUFFRDs7Ozs7Ozs7Ozs4Q0FPc0IsUyxFQUFVOztBQUU1QixtQkFBTyxJQUFJLE9BQUosQ0FBWSxVQUFDLE9BQUQsRUFBVSxNQUFWLEVBQXFCO0FBQ3BDLHFDQUFTLFFBQVQsaUJBQTZCLFNBQTdCLEVBQTBDLElBQTFDLENBQStDLG9CQUFZO0FBQ3ZELDJCQUFPLFFBQVEsUUFBUixDQUFQO0FBQ0gsaUJBRkQ7QUFHSCxhQUpNLENBQVA7QUFLSDs7Ozs7O0FBR0wsSUFBSSx1QkFBdUIsSUFBSSxXQUFKLEVBQTNCO2tCQUNlLG9COzs7Ozs7OztrQkNyRFMsSztBQVB4Qjs7Ozs7OztBQU9lLFNBQVMsS0FBVCxDQUFlLEdBQWYsRUFBbUI7QUFDOUIsV0FBTyxJQUFJLE9BQUosQ0FBWSxVQUFTLE9BQVQsRUFBa0IsTUFBbEIsRUFBeUI7O0FBRXhDLFlBQUksS0FBSyxNQUFNLEtBQUssS0FBTCxDQUFXLFFBQVEsS0FBSyxNQUFMLEVBQW5CLENBQWY7QUFDQSxZQUFJLGVBQWUsb0JBQW9CLEVBQXZDO0FBQ0EsZUFBTyxZQUFQLElBQXVCLFVBQUMsSUFBRCxFQUFRO0FBQzNCLG9CQUFRLElBQVI7QUFDQSxtQkFBTyxZQUFQLElBQXNCLFNBQXRCO0FBQ0gsU0FIRDs7QUFLQSxZQUFJLE1BQU0sT0FBTyxRQUFQLENBQWdCLG9CQUFoQixDQUFzQyxRQUF0QyxFQUFrRCxDQUFsRCxDQUFWO0FBQ0EsWUFBSSxTQUFTLE9BQU8sUUFBUCxDQUFnQixhQUFoQixDQUErQixRQUEvQixDQUFiO0FBQ0EsZUFBTyxHQUFQLEdBQWEsT0FBTyxJQUFJLE9BQUosQ0FBYSxHQUFiLElBQXFCLENBQXJCLEdBQXlCLEdBQXpCLEdBQStCLEdBQXRDLElBQTZDLFdBQTdDLEdBQTJELFlBQXhFOztBQUVBLFlBQUksVUFBSixDQUFlLFlBQWYsQ0FBNkIsTUFBN0IsRUFBcUMsR0FBckM7O0FBRUEsZUFBTyxNQUFQLEdBQWdCLFlBQVk7QUFDeEIsaUJBQUssTUFBTDtBQUNILFNBRkQ7QUFHSCxLQWxCTSxDQUFQO0FBbUJIIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsImltcG9ydCBkYXRhU2VydmljZSBmcm9tICcuL3NlcnZpY2VzL0RhdGFTZXJ2aWNlJztcbmltcG9ydCBDb21wb25lbnQgZnJvbSAnLi9jb21wb25lbnRzL0NvbXBvbmVudCc7XG5pbXBvcnQgQ2F0ZWdvcnkgZnJvbSAnLi9jb21wb25lbnRzL0NhdGVnb3J5JztcbmltcG9ydCBQcm9kdWN0IGZyb20gJy4vY29tcG9uZW50cy9Qcm9kdWN0JztcbmltcG9ydCBMaXN0VmlldyBmcm9tICcuL2NvbXBvbmVudHMvTGlzdFZpZXcnO1xuaW1wb3J0IE1vZGFsIGZyb20gJy4vY29tcG9uZW50cy9Nb2RhbCc7XG5cbi8qKlxuICogQXBwbGljYXRpb24gY2xhc3NcbiAqXG4gKi9cbmNsYXNzIEFwcCBleHRlbmRzICBDb21wb25lbnR7XG5cbiAgICAvKipcbiAgICAgKiBDb21wb25lbnQgY29uZmlndXJhdGlvbi5cbiAgICAgKlxuICAgICAqIEByZXR1cm5zIHt7Y29udGVudDogc3RyaW5nfX1cbiAgICAgKi9cbiAgICBpbml0KCl7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBjb250ZW50OmBcbiAgICAgICAgPGRpdiBpZD1cImNhdGVnb3JpZXNcIj48L2Rpdj5cblx0XHQ8ZGl2IGlkPVwicHJvZHVjdHNcIj48L2Rpdj5cblx0XHQ8ZGl2IGlkPVwibW9kYWxcIj48L2Rpdj5gXG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZW5lcmF0ZSBIVE1MIGVsZW1lbnQuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gY29udGFpbmVySWQgLSBDb250YWluZXIgSWQgZm9yIGFwcGxpY2F0aW9uLlxuICAgICAqL1xuICAgIHJlbmRlcihjb250YWluZXJJZCl7XG4gICAgICAgIHN1cGVyLnJlbmRlcih7Y29udGFpbmVySWR9KTtcbiAgICAgICAgbGV0IG1vZGFsID0gbmV3IE1vZGFsKCdtb2RhbCcpO1xuICAgICAgICBsZXQgcHJvZHVjdExpc3RzID1uZXcgTGlzdFZpZXcoJ3Byb2R1Y3RzJyxQcm9kdWN0LChwcm9kdWN0KT0+e1xuICAgICAgICAgICAgbW9kYWwucmVuZGVyKGRhdGFTZXJ2aWNlLmdldFByb2R1Y3RJbmZvcm1hdGlvbihwcm9kdWN0LmdldFZhbHVlKCdza3UnKSkpO1xuICAgICAgICB9KTtcbiAgICAgICAgbGV0IHNpZGVCYXJMaXN0ID1uZXcgTGlzdFZpZXcoJ2NhdGVnb3JpZXMnLENhdGVnb3J5LChjYXRlZ29yeSk9PntcbiAgICAgICAgICAgIHByb2R1Y3RMaXN0cy5yZW5kZXIoZGF0YVNlcnZpY2UuZ2V0Q2F0ZWdvcnlQcm9kdWN0cyhjYXRlZ29yeS5nZXRWYWx1ZSgnaWQnKSkpO1xuICAgICAgICB9KTtcblxuICAgICAgICBsZXQgYWxsQ2F0ZWdvcmllcyA9ZGF0YVNlcnZpY2UuZ2V0Q2F0ZWdvcmllcygpO1xuICAgICAgICBsZXQgYWxsUHJvZHVjdHM9IGRhdGFTZXJ2aWNlLmdldENhdGVnb3J5UHJvZHVjdHMoJ2RlcGFydG1lbnRzJyk7XG5cbiAgICAgICAgc2lkZUJhckxpc3QucmVuZGVyKGFsbENhdGVnb3JpZXMpO1xuICAgICAgICBwcm9kdWN0TGlzdHMucmVuZGVyKGFsbFByb2R1Y3RzKTtcbiAgICB9XG59XG5sZXQgYXBwID0gbmV3IEFwcCgpO1xuZXhwb3J0IGRlZmF1bHQgYXBwOyIsImltcG9ydCBDb21wb25lbnQgZnJvbSAnLi9Db21wb25lbnQnO1xuLyoqXG4gKiBDYXRlZ29yeSBjb21wb25lbnQgY2xhc3NcbiAqIHdpbGwgY3JlYXRlIGNhdGVnb3J5IGxpc3QgaXRlbVxuICpcbiAqL1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ2F0ZWdvcnkgZXh0ZW5kcyBDb21wb25lbnR7XG4gICAgaW5pdCAoKXtcbiAgICAgICByZXR1cm57XG4gICAgICAgICAgIGNvbnRlbnQ6IHRoaXMuZ2V0VmFsdWUoJ25hbWUnKSxcbiAgICAgICAgICAgY2xhc3NOYW1lOidjYXRlZ29yeS1pdGVtJ1xuICAgICAgIH1cbiAgIH1cbn1cbiIsIi8qKlxuICogTWFpbiBjbGFzcyBjb21wb25lbnQgY2xhc3NcbiAqL1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ29tcG9uZW50IHtcblxuICAgIC8qKlxuICAgICAqIENvbXBvbmVudCBjb25zdHJ1Y3RvclxuICAgICAqXG4gICAgICogQHBhcmFtIHt7fX0gICAgICAgZGF0YSAgICAgLSBDb21wb25lbnQgZGF0YS5cbiAgICAgKiBAcGFyYW0ge2Z1bmN0aW9ufSBjYWxsYmFjayAtIENsaWNrIGNhbGxiYWNrIGZ1bmN0aW9uXG4gICAgICovXG4gICAgY29uc3RydWN0b3IoZGF0YSwgY2FsbGJhY2sgPSBudWxsKSB7XG4gICAgICAgIGxldCBkZWZhdWx0cyA9IHtcbiAgICAgICAgICAgIHRhZzogJ2RpdicsXG4gICAgICAgICAgICBjbGFzc05hbWU6ICcnLFxuICAgICAgICAgICAgY29udGVudDogJycsXG4gICAgICAgICAgICBvbkNsaWNrOiBjYWxsYmFja1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLmRhdGEgPSBkYXRhO1xuICAgICAgICB0aGlzLnNldHRpbmdzID0gT2JqZWN0LmFzc2lnbih7fSwgZGVmYXVsdHMsIHRoaXMuaW5pdCgpKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBFeHRyYSBjb21wb25lbnQgY29uZmlndXJhdGlvbi5cbiAgICAgKlxuICAgICAqIEByZXR1cm5zIHt7fX1cbiAgICAgKi9cbiAgICBpbml0KCkge1xuICAgICAgICByZXR1cm4ge307XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0IGRhdGEgdmFsdWVcbiAgICAgKlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBrZXkgLSBUaGUga2V5IG5hbWUuXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7Kn1cbiAgICAgKi9cbiAgICBnZXRWYWx1ZShrZXkpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGF0YVtrZXldO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdlbmVyYXRlIEhUTUwgZWxlbWVudC5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSAgY29udGFpbmVySWQgLSBDb250YWluZXIgSWQgZm9yIHRoaXMgY29tcG9uZW50LlxuICAgICAqIEBwYXJhbSB7Ym9vbGVhbn0gYXBwZW5kICAgICAgLSBBcHBlbmQgZmxhZyBhbGxvdyBhZGQgdG8gZXhpc3QgY29udGVudC5cbiAgICAgKlxuICAgICAqIEByZXR1cm5zIHtFbGVtZW50fVxuICAgICAqL1xuICAgIHJlbmRlcih7Y29udGFpbmVySWQsIGFwcGVuZCA9IGZhbHNlfT17fSkge1xuICAgICAgICB2YXIgZWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQodGhpcy5zZXR0aW5ncy50YWcpO1xuICAgICAgICBlbGVtZW50LmlubmVySFRNTCA9IHRoaXMuc2V0dGluZ3MuY29udGVudDtcbiAgICAgICAgZWxlbWVudC5jbGFzc05hbWUgPSB0aGlzLnNldHRpbmdzLmNsYXNzTmFtZTtcbiAgICAgICAgaWYgKHR5cGVvZiB0aGlzLnNldHRpbmdzLmlkICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgZWxlbWVudC5pZCA9IHRoaXMuc2V0dGluZ3MuaWQ7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuc2V0dGluZ3Mub25DbGljayAhPSBudWxsKSB7XG4gICAgICAgICAgICBlbGVtZW50W3dpbmRvdy5hZGRFdmVudExpc3RlbmVyID8gJ2FkZEV2ZW50TGlzdGVuZXInIDogJ2F0dGFjaEV2ZW50J10od2luZG93LmFkZEV2ZW50TGlzdGVuZXIgPyAnY2xpY2snIDogJ29uY2xpY2snLCAoZXZlbnQpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLnNldHRpbmdzLm9uQ2xpY2soLi4uW3RoaXMsIGV2ZW50XSk7XG4gICAgICAgICAgICB9LCBmYWxzZSk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHR5cGVvZiBjb250YWluZXJJZCAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgIGxldCBjb250YWluZXIgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChjb250YWluZXJJZCk7XG4gICAgICAgICAgICBpZiAoIWFwcGVuZCkge1xuICAgICAgICAgICAgICAgIGNvbnRhaW5lci5pbm5lckhUTUwgPSAnJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNvbnRhaW5lci5hcHBlbmRDaGlsZChlbGVtZW50KTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBlbGVtZW50O1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFN0b3AgZXZlbnQgcHJvcGFnYXRpb24uXG4gICAgICpcbiAgICAgKiBAcGFyYW0gZXZlbnRcbiAgICAgKi9cbiAgICBzdG9wUHJvcGFnYXRpb24oZXZlbnQpIHtcbiAgICAgICAgaWYgKGV2ZW50LnN0b3BQcm9wYWdhdGlvbilldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgZWxzZSBldmVudC5jYW5jZWxCdWJibGUgPSB0cnVlO1xuICAgIH1cbn1cbiIsIi8qKlxuICogTGlzdCBWaWV3IGNsYXNzXG4gKi9cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIExpc3RWaWV3IHtcbiAgICAvKipcbiAgICAgKiBMaXN0IHZpZXcgY29uc3RydWN0b3IuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gICBjb250YWluZXJJZCAtIENvbnRhaW5lciBJZCBmb3IgdGhpcyBsaXN0IHZpZXcuXG4gICAgICogQHBhcmFtIHtzdHJpbmd9ICAgaXRlbUNsYXNzICAgLSBJdGVtJ3MgY2xhc3MgZm9yIHRoaXMgbGlzdCB2aWV3LlxuICAgICAqIEBwYXJhbSB7ZnVuY3Rpb259IGNhbGxCYWNrICAgIC0gY2FsbGJhY2sgZXZlbnQgZnVuY3Rpb24uXG4gICAgICovXG4gICAgY29uc3RydWN0b3IoY29udGFpbmVySWQsIGl0ZW1DbGFzcywgY2FsbEJhY2spIHtcbiAgICAgICAgdGhpcy5pdGVtQ2xhc3MgPSBpdGVtQ2xhc3M7XG4gICAgICAgIHRoaXMuY29udGFpbmVySWQgPSBjb250YWluZXJJZDtcbiAgICAgICAgdGhpcy5jYWxsQmFjayA9IGNhbGxCYWNrO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdlbmVyYXRlIEhUTUwgZWxlbWVudCB3aXRoIG5vZGVzIG9mIGNvbXBvbmVudHMgYmFzZSBvbiBpdGVtQ2xhc3NcbiAgICAgKlxuICAgICAqIEBwYXJhbSB7UHJvbWlzZX0gZGF0YVByb21pc2VcbiAgICAgKi9cbiAgICByZW5kZXIoZGF0YVByb21pc2UpIHtcbiAgICAgICAgbGV0IGNvbnRhaW5lciA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKHRoaXMuY29udGFpbmVySWQpO1xuICAgICAgICBjb250YWluZXIuaW5uZXJIVE1MID0gJzxkaXYgY2xhc3M9XCJsb2FkZXJcIj48L2Rpdj4nO1xuICAgICAgICBkYXRhUHJvbWlzZS50aGVuKChkYXRhKT0+IHtcbiAgICAgICAgICAgICAgICBjb250YWluZXIuaW5uZXJIVE1MID0gJyc7XG4gICAgICAgICAgICAgICAgZGF0YS5tYXAoaXRlbSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGxldCBfaXRlbSA9IG5ldyB0aGlzLml0ZW1DbGFzcyhpdGVtLCB0aGlzLmNhbGxCYWNrKTtcbiAgICAgICAgICAgICAgICAgICAgY29udGFpbmVyLmFwcGVuZENoaWxkKF9pdGVtLnJlbmRlcigpKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgKVxuICAgIH1cbn0iLCJpbXBvcnQgQ29tcG9uZW50IGZyb20gJy4vQ29tcG9uZW50JztcbmltcG9ydCBQcm9kdWN0RGV0YWlsIGZyb20gJy4vUHJvZHVjdERldGFpbCc7XG5cbi8qKlxuICogUHJvZHVjdCBNb2RhbCBWaWV3LlxuICovXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBNb2RhbCBleHRlbmRzIENvbXBvbmVudCB7XG5cbiAgICAvKipcbiAgICAgKiBQcm9kdWN0IE1vZGFsIGNvbnN0cnVjdG9yLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGNvbnRhaW5lcklkIC0gQ29udGFpbmVyIElkIGZvciB0aGlzIE1vZGFsLlxuICAgICAqL1xuICAgIGNvbnN0cnVjdG9yKGNvbnRhaW5lcklkKSB7XG4gICAgICAgIHN1cGVyKCk7XG4gICAgICAgIHRoaXMuY29udGFpbmVySWQgPSBjb250YWluZXJJZDtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBDb21wb25lbnQgY29uZmlndXJhdGlvblxuICAgICAqXG4gICAgICogQHJldHVybnMge3tpZDogc3RyaW5nLCBjb250ZW50OiBzdHJpbmcsIG9uQ2xpY2s6IChmdW5jdGlvbigpKX19XG4gICAgICovXG4gICAgaW5pdCgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGlkOiAnb3ZlcmxheScsXG4gICAgICAgICAgICBjb250ZW50OiBgPGRpdiBpZD1cInByb2R1Y3QtbW9kYWxcIj48ZGl2IGNsYXNzPVwibG9hZGVyXCI+PC9kaXY+PC9kaXY+YCxcbiAgICAgICAgICAgIG9uQ2xpY2s6ICgpPT4ge1xuICAgICAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKHRoaXMuY29udGFpbmVySWQpLmlubmVySFRNTCA9ICcnO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2VuZXJhdGUgSFRNTCBlbGVtZW50LlxuICAgICAqXG4gICAgICogQHBhcmFtIHtQcm9taXNlfSBkYXRhUHJvbWlzZVxuICAgICAqL1xuICAgIHJlbmRlcihkYXRhUHJvbWlzZSkge1xuICAgICAgICBzdXBlci5yZW5kZXIoe2NvbnRhaW5lcklkOiB0aGlzLmNvbnRhaW5lcklkfSk7XG4gICAgICAgIGRhdGFQcm9taXNlLnRoZW4oKGRhdGEpPT4ge1xuICAgICAgICAgICAgICAgIGxldCBwcm9kdWN0RGV0YWlsID0gbmV3IFByb2R1Y3REZXRhaWwoZGF0YSwgKGEsIGUpPT4ge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnN0b3BQcm9wYWdhdGlvbihlKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICBwcm9kdWN0RGV0YWlsLnJlbmRlcih7Y29udGFpbmVySWQ6ICdwcm9kdWN0LW1vZGFsJ30pXG4gICAgICAgICAgICB9XG4gICAgICAgIClcbiAgICB9XG59IiwiaW1wb3J0IENvbXBvbmVudCBmcm9tICcuL0NvbXBvbmVudCc7XG4vKipcbiAqIFByb2R1Y3QgY2xhc3NcbiAqL1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUHJvZHVjdCBleHRlbmRzIENvbXBvbmVudCB7XG4gICAgLyoqXG4gICAgICogQ29tcG9uZW50IGNvbmZpZ3VyYXRpb24uXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7e2NvbnRlbnQ6IHN0cmluZywgY2xhc3NOYW1lOiBzdHJpbmd9fVxuICAgICAqL1xuICAgIGluaXQoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBjb250ZW50OiBgPGRpdiBjbGFzcz1cInByb2R1Y3QtaW1hZ2VcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz1cIiR7dGhpcy5nZXRWYWx1ZSgndGh1bWJuYWlsSW1hZ2UnKX1cIiBhbHQ9XCIke3RoaXMuZ2V0VmFsdWUoJ25hbWUnKX1cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicHJvZHVjdC1uYW1lXCI+JHt0aGlzLmdldFZhbHVlKCduYW1lJyl9PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInByb2R1Y3QtcHJpY2VcIj48c3Ryb25nPiQke3RoaXMuZ2V0VmFsdWUoJ3JlZ3VsYXJQcmljZScpfTwvc3Ryb25nPjwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGAsXG4gICAgICAgICAgICBjbGFzc05hbWU6ICdwcm9kdWN0LWl0ZW0nXG4gICAgICAgIH1cbiAgICB9XG59IiwiaW1wb3J0IENvbXBvbmVudCBmcm9tICcuL0NvbXBvbmVudCc7XG4vKipcbiAqIFByb2R1Y3REZXRhaWwgY2xhc3NcbiAqL1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUHJvZHVjdERldGFpbCBleHRlbmRzIENvbXBvbmVudCB7XG4gICAgLyoqXG4gICAgICogQ29tcG9uZW50IGNvbmZpZ3VyYXRpb24uXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7e2NvbnRlbnQ6IHN0cmluZywgY2xhc3NOYW1lOiBzdHJpbmd9fVxuICAgICAqL1xuICAgIGluaXQoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBjb250ZW50OiBgPGRpdiBjbGFzcz1cInByb2R1Y3QtbmFtZVwiPjxoMT4ke3RoaXMuZ2V0VmFsdWUoJ25hbWUnKX08L2gxPjwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJwcm9kdWN0LWltYWdlXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz1cIiR7dGhpcy5nZXRWYWx1ZSgndGh1bWJuYWlsSW1hZ2UnKX1cIiBhbHQ9XCIke3RoaXMuZ2V0VmFsdWUoJ25hbWUnKX1cIj5cbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicHJvZHVjdC1kZXNjcmlwdGlvblwiPiR7dGhpcy5nZXRWYWx1ZSgnc2hvcnREZXNjcmlwdGlvbicpfTwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJwcm9kdWN0LXByaWNlXCI+PGgyPiQke3RoaXMuZ2V0VmFsdWUoJ3JlZ3VsYXJQcmljZScpfTwvaDI+PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgIGAsXG4gICAgICAgICAgICBjbGFzc05hbWU6ICdwcm9kdWN0LWRldGFpbCdcbiAgICAgICAgfVxuICAgIH1cbn0iLCJpbXBvcnQgQXBwIGZyb20gJy4vQXBwJztcbkFwcC5yZW5kZXIoJ2FwcC1jb250YWluZXInKTtcbiIsImltcG9ydCBqc29ucCBmcm9tICcuL2pzb25wJztcbi8qKlxuICogQmFzZSBBUEkgVXJsXG4gKiBAdHlwZSB7c3RyaW5nfVxuICovXG5jb25zdCBCQVNFX1VSTD0naHR0cDovL3d3dy5iZXN0YnV5LmNhL2FwaS92Mi9qc29uJztcblxuLyoqXG4gKiBEYXRhU2VydmljZSBjbGFzcy5cbiAqL1xuY2xhc3MgRGF0YVNlcnZpY2V7XG5cbiAgICAvKipcbiAgICAgKiBHZXQgQWxsIGNhdGVnb3JpZXMuXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZX1cbiAgICAgKi9cbiAgICBnZXRDYXRlZ29yaWVzKCl7XG5cbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgICAgICAgIGpzb25wKGAke0JBU0VfVVJMfS9jYXRlZ29yeS9EZXBhcnRtZW50c2ApLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVzb2x2ZShyZXNwb25zZS5zdWJDYXRlZ29yaWVzKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0IGNhdGVnb3J5IHByb2R1Y3RzLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGNhdGVnb3J5SWQgLSBUaGUgY2F0ZWdvcnkgaWRcbiAgICAgKlxuICAgICAqIEByZXR1cm5zIHtQcm9taXNlfVxuICAgICAqL1xuICAgIGdldENhdGVnb3J5UHJvZHVjdHMoY2F0ZWdvcnlJZCl7XG5cbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgICAgICAgIGpzb25wKGAke0JBU0VfVVJMfS9zZWFyY2g/Y2F0ZWdvcnlJZD0ke2NhdGVnb3J5SWR9YCkudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHJlc29sdmUocmVzcG9uc2UucHJvZHVjdHMpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldCBwcm9kdWN0IEluZm9ybWF0aW9uLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHByb2R1Y3RJZCA9IFRoZSBwcm9kdWN0IElkLlxuICAgICAqXG4gICAgICogQHJldHVybnMge1Byb21pc2V9XG4gICAgICovXG4gICAgZ2V0UHJvZHVjdEluZm9ybWF0aW9uKHByb2R1Y3RJZCl7XG5cbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgICAgICAgIGpzb25wKGAke0JBU0VfVVJMfS9wcm9kdWN0LyR7cHJvZHVjdElkfWApLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiByZXNvbHZlKHJlc3BvbnNlKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG59XG5cbmxldCBTaW5nbGV0b25EYXRhU2VydmljZSA9IG5ldyBEYXRhU2VydmljZSgpO1xuZXhwb3J0IGRlZmF1bHQgU2luZ2xldG9uRGF0YVNlcnZpY2U7IiwiLyoqXG4gKiBBamF4IHJlcXVlc3QgdXRpbGl0eVxuICpcbiAqIEBwYXJhbSB7c3RyaW5nfSB1cmkgLSBUaGUgcmVzb3VyY2UgdXJpXG4gKlxuICogQHJldHVybnMge1Byb21pc2V9XG4gKi9cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGpzb25wKHVyaSl7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uKHJlc29sdmUsIHJlamVjdCl7XG5cbiAgICAgICAgdmFyIGlkID0gJ18nICsgTWF0aC5yb3VuZCgxMDAwMCAqIE1hdGgucmFuZG9tKCkpO1xuICAgICAgICB2YXIgY2FsbGJhY2tOYW1lID0gJ2pzb25wX2NhbGxiYWNrXycgKyBpZDtcbiAgICAgICAgd2luZG93W2NhbGxiYWNrTmFtZV0gPSAoZGF0YSk9PntcbiAgICAgICAgICAgIHJlc29sdmUoZGF0YSk7XG4gICAgICAgICAgICB3aW5kb3dbY2FsbGJhY2tOYW1lXSA9dW5kZWZpbmVkO1xuICAgICAgICB9O1xuXG4gICAgICAgIHZhciByZWYgPSB3aW5kb3cuZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoICdzY3JpcHQnIClbIDAgXTtcbiAgICAgICAgdmFyIHNjcmlwdCA9IHdpbmRvdy5kb2N1bWVudC5jcmVhdGVFbGVtZW50KCAnc2NyaXB0JyApO1xuICAgICAgICBzY3JpcHQuc3JjID0gdXJpICsgKHVyaS5pbmRleE9mKCAnPycgKSArIDEgPyAnJicgOiAnPycpICsgJ2NhbGxiYWNrPScgKyBjYWxsYmFja05hbWU7XG5cbiAgICAgICAgcmVmLnBhcmVudE5vZGUuaW5zZXJ0QmVmb3JlKCBzY3JpcHQsIHJlZiApO1xuXG4gICAgICAgIHNjcmlwdC5vbmxvYWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB0aGlzLnJlbW92ZSgpO1xuICAgICAgICB9O1xuICAgIH0pXG59Il19
