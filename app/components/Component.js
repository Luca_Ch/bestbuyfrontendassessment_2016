/**
 * Main class component class
 */
export default class Component {

    /**
     * Component constructor
     *
     * @param {{}}       data     - Component data.
     * @param {function} callback - Click callback function
     */
    constructor(data, callback = null) {
        let defaults = {
            tag: 'div',
            className: '',
            content: '',
            onClick: callback
        };
        this.data = data;
        this.settings = Object.assign({}, defaults, this.init());
    }

    /**
     * Extra component configuration.
     *
     * @returns {{}}
     */
    init() {
        return {};
    }

    /**
     * Get data value
     *
     * @param {string} key - The key name.
     *
     * @returns {*}
     */
    getValue(key) {
        return this.data[key];
    }

    /**
     * Generate HTML element.
     *
     * @param {string}  containerId - Container Id for this component.
     * @param {boolean} append      - Append flag allow add to exist content.
     *
     * @returns {Element}
     */
    render({containerId, append = false}={}) {
        var element = document.createElement(this.settings.tag);
        element.innerHTML = this.settings.content;
        element.className = this.settings.className;
        if (typeof this.settings.id !== 'undefined') {
            element.id = this.settings.id;
        }
        if (this.settings.onClick != null) {
            element[window.addEventListener ? 'addEventListener' : 'attachEvent'](window.addEventListener ? 'click' : 'onclick', (event) => {
                this.settings.onClick(...[this, event]);
            }, false);
        }
        if (typeof containerId !== 'undefined') {
            let container = document.getElementById(containerId);
            if (!append) {
                container.innerHTML = '';
            }
            container.appendChild(element);
        }

        return element;
    }

    /**
     * Stop event propagation.
     *
     * @param event
     */
    stopPropagation(event) {
        if (event.stopPropagation)event.stopPropagation();
        else event.cancelBubble = true;
    }
}
