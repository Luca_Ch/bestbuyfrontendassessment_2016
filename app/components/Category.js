import Component from './Component';
/**
 * Category component class
 * will create category list item
 *
 */
export default class Category extends Component{
    init (){
       return{
           content: this.getValue('name'),
           className:'category-item'
       }
   }
}
