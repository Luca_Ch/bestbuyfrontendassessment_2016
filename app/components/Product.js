import Component from './Component';
/**
 * Product class
 */
export default class Product extends Component {
    /**
     * Component configuration.
     *
     * @returns {{content: string, className: string}}
     */
    init() {
        return {
            content: `<div class="product-image">
                            <img src="${this.getValue('thumbnailImage')}" alt="${this.getValue('name')}">
                            </div>
                            <div class="product-name">${this.getValue('name')}</div>
                            <div class="product-price"><strong>$${this.getValue('regularPrice')}</strong></div>
                            `,
            className: 'product-item'
        }
    }
}