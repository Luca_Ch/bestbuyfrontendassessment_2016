import Component from './Component';
/**
 * ProductDetail class
 */
export default class ProductDetail extends Component {
    /**
     * Component configuration.
     *
     * @returns {{content: string, className: string}}
     */
    init() {
        return {
            content: `<div class="product-name"><h1>${this.getValue('name')}</h1></div>
                      <div class="product-image">
                        <img src="${this.getValue('thumbnailImage')}" alt="${this.getValue('name')}">
                      </div>
                      <div class="product-description">${this.getValue('shortDescription')}</div>
                      <div class="product-price"><h2>$${this.getValue('regularPrice')}</h2></div>
                       `,
            className: 'product-detail'
        }
    }
}