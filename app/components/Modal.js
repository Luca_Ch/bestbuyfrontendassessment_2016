import Component from './Component';
import ProductDetail from './ProductDetail';

/**
 * Product Modal View.
 */
export default class Modal extends Component {

    /**
     * Product Modal constructor.
     *
     * @param {string} containerId - Container Id for this Modal.
     */
    constructor(containerId) {
        super();
        this.containerId = containerId;
    }

    /**
     * Component configuration
     *
     * @returns {{id: string, content: string, onClick: (function())}}
     */
    init() {
        return {
            id: 'overlay',
            content: `<div id="product-modal"><div class="loader"></div></div>`,
            onClick: ()=> {
                document.getElementById(this.containerId).innerHTML = '';
            }
        }
    }

    /**
     * Generate HTML element.
     *
     * @param {Promise} dataPromise
     */
    render(dataPromise) {
        super.render({containerId: this.containerId});
        dataPromise.then((data)=> {
                let productDetail = new ProductDetail(data, (a, e)=> {
                    this.stopPropagation(e);
                });
                productDetail.render({containerId: 'product-modal'})
            }
        )
    }
}