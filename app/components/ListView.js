/**
 * List View class
 */
export default class ListView {
    /**
     * List view constructor.
     *
     * @param {string}   containerId - Container Id for this list view.
     * @param {string}   itemClass   - Item's class for this list view.
     * @param {function} callBack    - callback event function.
     */
    constructor(containerId, itemClass, callBack) {
        this.itemClass = itemClass;
        this.containerId = containerId;
        this.callBack = callBack;
    }

    /**
     * Generate HTML element with nodes of components base on itemClass
     *
     * @param {Promise} dataPromise
     */
    render(dataPromise) {
        let container = document.getElementById(this.containerId);
        container.innerHTML = '<div class="loader"></div>';
        dataPromise.then((data)=> {
                container.innerHTML = '';
                data.map(item => {
                    let _item = new this.itemClass(item, this.callBack);
                    container.appendChild(_item.render());
                });
            }
        )
    }
}