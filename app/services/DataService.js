import jsonp from './jsonp';
/**
 * Base API Url
 * @type {string}
 */
const BASE_URL='http://www.bestbuy.ca/api/v2/json';

/**
 * DataService class.
 */
class DataService{

    /**
     * Get All categories.
     *
     * @returns {Promise}
     */
    getCategories(){

        return new Promise((resolve, reject) => {
            jsonp(`${BASE_URL}/category/Departments`).then(response => {
                    return resolve(response.subCategories);
                });
        });
    }

    /**
     * Get category products.
     *
     * @param {string} categoryId - The category id
     *
     * @returns {Promise}
     */
    getCategoryProducts(categoryId){

        return new Promise((resolve, reject) => {
            jsonp(`${BASE_URL}/search?categoryId=${categoryId}`).then(response => {
                return resolve(response.products);
            });
        });
    }

    /**
     * Get product Information.
     *
     * @param {string} productId = The product Id.
     *
     * @returns {Promise}
     */
    getProductInformation(productId){

        return new Promise((resolve, reject) => {
            jsonp(`${BASE_URL}/product/${productId}`).then(response => {
                return resolve(response);
            });
        });
    }
}

let SingletonDataService = new DataService();
export default SingletonDataService;