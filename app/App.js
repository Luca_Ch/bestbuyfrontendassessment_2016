import dataService from './services/DataService';
import Component from './components/Component';
import Category from './components/Category';
import Product from './components/Product';
import ListView from './components/ListView';
import Modal from './components/Modal';

/**
 * Application class
 *
 */
class App extends  Component{

    /**
     * Component configuration.
     *
     * @returns {{content: string}}
     */
    init(){
        return {
            content:`
        <div id="categories"></div>
		<div id="products"></div>
		<div id="modal"></div>`
        }
    }

    /**
     * Generate HTML element.
     *
     * @param {string} containerId - Container Id for application.
     */
    render(containerId){
        super.render({containerId});
        let modal = new Modal('modal');
        let productLists =new ListView('products',Product,(product)=>{
            modal.render(dataService.getProductInformation(product.getValue('sku')));
        });
        let sideBarList =new ListView('categories',Category,(category)=>{
            productLists.render(dataService.getCategoryProducts(category.getValue('id')));
        });

        let allCategories =dataService.getCategories();
        let allProducts= dataService.getCategoryProducts('departments');

        sideBarList.render(allCategories);
        productLists.render(allProducts);
    }
}
let app = new App();
export default app;